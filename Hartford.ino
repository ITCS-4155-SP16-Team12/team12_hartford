
#include <SPI.h>
#include <SD.h>
#include <ESP8266WiFi.h>
#include <math.h>
#define adcPin A0

const int SAMPLE_WINDOW = 50; 	// Sample Window in 50ms = 20Hz
const int CHIP_SELECT = 15; 		// SD Chip Select Pin
const int SOUND_LV_THR = 9; 	  // dangerous sound level threshold
const int TIME_DUR_LIMIT = 2500; 		// Sound duration warning
const String LOG_FILE_NAME = "Demo_Logs.txt"; // Name of File

const char* ssid = "NinerWiFi-Guest"; // Name of WiFi Network
const char* password = "";            // WiFi Password

String apiKey = "Z8RW9PAC8706DKMW";
const char* server = "api.thingspeak.com";
WiFiClient client;

File logFile;
boolean sdInit = false;
unsigned long duration;
double db;

boolean writeToSD(unsigned long millis, double thresh); // Writes to log file on SD
boolean writeToServer(unsigned long millis, double thresh); // Writes to the server
double sampleSound(); // Samples the sound levels

void setup() {
  pinMode(16, OUTPUT);  // LED 1
  pinMode(5, OUTPUT);   // LED 2
  pinMode(4, OUTPUT);   // LED 3
	Serial.begin(115200);
	while (!Serial); // Wait for serial communication
	
	// SD Init
	Serial.println("SD init....");
	if (!SD.begin(CHIP_SELECT)) {
		Serial.println("SD init failed!");
		return;
	} else sdInit = true;
	
	Serial.println("SD init done.");
  
  //Connect to network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Listening...");
}

void loop() {
  if (sdInit) {
  	double soundLevel = sampleSound();
  	int intSoundLevel = (int)soundLevel;
  	
  	if (intSoundLevel > 8) {
      Serial.println("Hazardous Noise Level Detected!");
  		duration = millis();
  		while ((int)sampleSound() > 8) {
  		  soundLevel = sampleSound();
  			// keep sampling sound to take duration of hazardous noise
        if ((millis() - duration > TIME_DUR_LIMIT)) 
          Serial.println("Hazardous Exposure Time!");
        delay(25);
  		}
  		
  		duration = millis() - duration;
  		if (duration > TIME_DUR_LIMIT) {
  			if (writeToSD(duration, db)) {
  				Serial.println("SD Writing Successful.");
         
  			}
  			else Serial.println("SD Writing Failed.");
        if (writeToServer(duration, db))
          Serial.println("ThingsSpeak Writing Successful.");
        else Serial.println("ThingsSpeak Writing Failed.");
  		}
  	}
   delay(50);
   String dbString = String(db, 2);
   dbString = "DB:\t" + dbString;
   Serial.println(dbString);
  }
}

double sampleSound() {
	unsigned long startMillis = millis();   // Start of sample window
	unsigned int peakToPeak = 0;            // peak-to-peak level
 
	unsigned int signalMax = 0;
	unsigned int signalMin = 1024;
  unsigned int sample;

	// collect data for 50 mS
	while (millis() - startMillis < SAMPLE_WINDOW) {
		sample = analogRead(adcPin);
		if (sample < 1024) {  			// toss out spurious readings
			if (sample > signalMax){
				signalMax = sample;  	  // save just the max levels
			} else if (sample < signalMin) {
				signalMin = sample;  	  // save just the min levels
			}
		}
	}
	peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
	double volts = (peakToPeak * 10.0);
	volts = volts / 1024;

  db = 20.0 * log10(peakToPeak);
  db = 100 - abs(db);

  if ((int)volts > 8) {
    digitalWrite(16, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(4, HIGH);
  } else if ((int)volts > 4) {
    digitalWrite(16, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(4, LOW);
  } else if ((int)volts > 2) {
    digitalWrite(16, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(4, LOW);
  } else {
    digitalWrite(16, LOW);
    digitalWrite(5, LOW);
    digitalWrite(4, LOW);
  }
 
	return volts;
}

boolean writeToSD(unsigned long millis, double thresh) {
	logFile = SD.open("logs.txt", FILE_WRITE);
	if (logFile) {
		Serial.println("Writing to 'Demo_Log.txt:");
		
		String strTime;
		int fracTime, hours, minutes, seconds;
		
		unsigned long intTime = millis / 1000;
		fracTime = millis % 1000;

		if (intTime > 3599) {
			hours = intTime / 3600;
			minutes = intTime / 60;
			seconds = intTime;
			strTime = "H: " + strTime + String(hours, DEC) + "\t";
			strTime = "M: " + strTime + String(minutes, DEC) + "\t";
			strTime = "S: " + strTime + String(seconds, DEC) + "." + String(fracTime, DEC);
		} else if (intTime > 59) {
			minutes = intTime / 60;
			seconds = intTime;
			strTime = "M: " + strTime + String(minutes, DEC) + "\t";
			strTime = "S: " + strTime + String(seconds, DEC) + "." + String(fracTime, DEC);
		} else {
			seconds = intTime;
			strTime = "S: " + strTime + String(seconds, DEC) + "." + String(fracTime, DEC);
		}
		
		String strSoundLevel = String(thresh, 2);
		String strLog = "Sound Lv: " + strSoundLevel + "\tDuration: " + strTime;
		logFile.println(strLog);
		Serial.println(strLog);
		logFile.close();
		return true;
	} else 
		return false;
}

boolean writeToServer(unsigned long millis, double thresh) {
  if (client.connect(server,80)) {  //   "184.106.153.149" or api.thingspeak.com
    String postStr = apiKey;
           postStr +="&field1=";
           postStr += String(millis / 1000);
           postStr +="&field2=";
           postStr += String(thresh);
           postStr += "\r\n\r\n";
 
     client.print("POST /update HTTP/1.1\n"); 
     client.print("Host: api.thingspeak.com\n"); 
     client.print("Connection: close\n"); 
     client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n"); 
     client.print("Content-Type: application/x-www-form-urlencoded\n"); 
     client.print("Content-Length: "); 
     client.print(postStr.length()); 
     client.print("\n\n"); 
     client.print(postStr);   
  } else return false;
  client.stop();
   
  Serial.println("Waiting to server...");    
  // thingspeak needs minimum 15 sec delay between updates
  delay(16000);
  return true;
}

